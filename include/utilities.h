#ifndef __UTILITIES_H__
#define __UTILITIES_H__

#include <stdint.h>

double limitValue2(double value, double minValue, double maxValue);
double limitValue(double value, double maxValue);

#endif
