/*
 * File name: asctec_usb.h
 * Date:      2017/12/14 10:18
 * Author:    Jan Chudoba
 */

#ifndef __ASCTEC_USB_H__
#define __ASCTEC_USB_H__

#include <stdint.h>
#include <stdio.h>
#include <pthread.h>

#include <ros/ros.h>

#include <asctecData.h>
#include <utilities.h>

#include <mrs_msgs/HwApiStatus.h>

#define RC_GAIN 0.5
#define RC_CENTER_POS 2032
#define RC_RUDDER_CENTER_POS 2016

#define UAV_STATUS_MASK_ALTITUDE_CONTROL	0x0001
#define UAV_STATUS_MASK_HEIGHT_CONTROL		0x0002
#define UAV_STATUS_MASK_POSITION_CONTROL	0x0004
#define UAV_STATUS_MASK_FLYING				0x0008
#define UAV_STATUS_MASK_COMPASS_FAILURE		0x0010
#define UAV_STATUS_MASK_SERIAL_ENABLED		0x0020
#define UAV_STATUS_MASK_SERIAL_ACTIVE		0x0040
#define UAV_STATUS_MASK_EMERGENCY_MODE		0x0080
#define UAV_STATUS_MASK_CALIBRATION_ERROR	0x0100
#define UAV_STATUS_MASK_GYRO_CAL_ERROR		0x0200
#define UAV_STATUS_MASK_ACC_CAL_ERROR		0x0400
#define UAV_STATUS_MASK_MAG_FIELD_ERROR		0x4000
#define UAV_STATUS_MASK_MAG_INCL_ERROR		0x8000

//ctrl_flags
#define CTRL_PITCH							0x01
#define CTRL_ROLL							0x02
#define CTRL_YAW 							0x04
#define CTRL_THRUST							0x08
#define CTRL_HEIGHT_ENABLED					0x10
#define CTRL_GPS_ENABLED					0x20

struct SControlValues{
  double elevator, aileron, rudder, thrust;
};

struct SGpsSetPoint{
  double latitude, longitude, altitude, azimuth;  // y, x, z, yaw
};

struct SState{
  uint16_t status;
  bool armed;
  bool offboard;
};

class CAsctecUSB
{
public:
	CAsctecUSB();
	~CAsctecUSB();

	bool open(const char * device);
	void close();

	bool initialize(int rate = 0);

	bool checkDataUpdated() { bool result = externalDataUpdated; externalDataUpdated = false; return result; }

	void sendSimGps(double latitude, double longitude, double altitude, double headingDeg);
	void stopSimGps();

	bool arm();
	bool disarm();
	void enableCtrl();
	void disableCtrl();
	bool setControlMode(TAsctecControlMode mode);
	bool setControlDirect(SControlValues control); // values are <-1;1>, thrust <0;1>
	bool setControlAttitude(SControlValues control); // angle units: radians, thrust <0;1>
	bool setControlPosition(SGpsSetPoint setPoint, uint32_t * pIndex = NULL); // lat/lon [deg], alt[m], yaw[rad]

	bool getAttitude(CAttitudeRaw & resultAttitude);
	bool getGpsPosition(CGpsDataRaw & resultPosition);
	bool getFusedPosition(CGpsDataRaw & resultPosition);
	uint16_t getUavStatus() { return uavStatus; }
	double getUavBatteryVoltage() { return 1e-3 * (double) uavBatteryVoltage; }
	uint16_t getUavFlightTime() { return uavFlightTime; }
	bool uavIsFlying() { return ((uavStatus & UAV_STATUS_MASK_FLYING) != 0); }
	bool uavSerialControlEnabled() { return ((uavStatus & UAV_STATUS_MASK_SERIAL_ENABLED) != 0); }
	uint16_t getRcChannelRaw(int index) { return (index < 8) ? rcChannel[index] : 0; }
	uint32_t getGpsTimeOfWeek() { return gpsTimeOfWeek; }

	void setDebugLevel(int level = 1) { debugLevel = level; }
	void setReadMotorRpm(bool enable) { readMotorRpm = enable; } // need to be called before initialize()
	void disableGps() { noGpsMode = true; }

	uint8_t getMotorRpm(int index) { return motorRPM[index]; }

	// setting of callbacks
	void setIMUCallback(void (*callback)(CAttitude&, void*), void* uInput);
	void setRCCallback(void (*callback)(SControlValues&, void*), void* uInput);
	void setStateCallback(void (*callback)(SState&, void*), void* uInput);

private:
	static void * thread_body(void * arg) { ((CAsctecUSB*)arg)->threadBody(); return 0; }
	void threadBody();

	static CAsctecUSB * instance;
	static void versionsUpdated(struct ACI_INFO aciInfo);
	static void varListUpdateFinished();
	static void cmdListUpdateFinished();
	static void paramListUpdateFinished();
	static void varPacketReceived(unsigned char packetNumber);
	static void transmit(void* byte, unsigned short cnt);

	void initializeCommands();
	void initializeVariables();
	void initializeParameters();
	void onVarPacketReceived(unsigned char packetNumber);

	// callbacks
	void (*IMUCallback)(CAttitude&, void* uInput);
	void* IMUCallInput;
	void (*RCCallback)(SControlValues&, void* uInput);
	void* RCCallInput;
	void (*StateCallback)(SState&, void* uInput);
	void* StateCallInput;

	int fd;
	pthread_t thread;
	bool threadInterrupt;
	bool threadStarted;

	bool commandsReady;
	bool variablesReady;
	bool parametersReady;

	bool internalDataUpdated;
	bool externalDataUpdated;

	CSimGpsData simGpsData;
	CGpsDataRaw gpsData;

	unsigned short variableRequestPeriodMs;

	/*
	 * uavStatus bits: (copied from asctec-sdk/sdk.h)
	 * 0x01 ATTITUDE CONTROL
	 * 0x02 HEIGHT CONTROL
	 * 0x04 POSITION CONTROL
	 * 0x04 FLYING (added by HCH in sdk)
	 * 0x10 COMPASS FAILURE
	 * 0x20 SERIAL INTERFACE ENABLED
	 * 0x40 SERIAL INTERFACE ACTIVE //is active when control commands are sent to the LL
	 * 0x80 EMERGENCY MODE //when RC link is lost -> serial interface disabled
	 * 0x100 CALIBRATION ERROR
	 * 0x200 GYRO CALIBRATION ERROR
	 * 0x400 ACC CALIBRATION ERROR
	 * 0x4000 MAGNETIC FIELD STRENGTH ERROR
	 * 0x8000 MAGNETIC INCLINATION ERROR
	 *
	 * example: 0x07 means GPS Mode activated (ATTITUDE-, HEIGHT- and POSITION CONTROL active)
	 */
	uint16_t uavStatus;
	uint16_t uavFlightTime; // [s]
	uint16_t uavBatteryVoltage; // [mV]

	CGpsDataRaw gpsPosition;
	CGpsDataRaw fusedPosition;
	CAttitudeRaw attitude;
	uint32_t gpsTimeOfWeek;
	uint8_t motorRPM[6];
	bool readMotorRpm;
	bool noGpsMode;
	bool armed;

	uint16_t rcChannel[8]; // 0..4095; 2048 ~ center
	// channels are: 0=pitch, 1=roll, 2=thrust, 3=yaw, 4=serial en(>2048)/dis, 5=ctrl_mode, 6=custom, 7=custom

	uint8_t ctrlMode;
	uint8_t ctrlEnabled;
	int16_t ctrlPitchInput; // -2047 - 2047; max value ~ 51.2 deg (non-GPS mode) / ~ 3m/s in GPS mode
	int16_t ctrlRollInput; // -2047 - 2047
	int16_t ctrlYawInput; // -2047 - 2047
	int16_t ctrlThrustInput; // 0 - 4095
	int16_t ctrlInputCtrl; // bitmask: 0=pitch, 1=roll, 2=yaw, 3=thrust, 4=height, 5=gps-pos
	uint8_t dmcPitch; // 0 - 200
	uint8_t dmcRoll; // 0 - 200
	uint8_t dmcYaw; // 0 - 200
	uint8_t dmcThrust; // 0 - 200

	uint8_t waypointUpdate;
	int32_t waypointLatitude;
	int32_t waypointLongitude;
	int32_t waypointAltitude;
	int32_t waypointHeading;
	uint32_t waypointIndex;
	uint32_t waypointIndexGenerator;

	uint16_t paramMaxCruiseSpeed;
	uint16_t paramPositionAccuracy;

	int debugLevel;

	FILE * fEngineLog;
};

#endif

/* end of asctec_usb.h */
