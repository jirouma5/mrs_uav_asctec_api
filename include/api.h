#ifndef __API_H__
#define __API_H__

/* includes //{ */

#include <ros/ros.h>

#include <mrs_uav_hw_api/api.h>

#include <mrs_lib/param_loader.h>
#include <mrs_lib/attitude_converter.h>
#include <mrs_lib/mutex.h>
#include <mrs_lib/publisher_handler.h>
#include <mrs_lib/subscribe_handler.h>

#include <asctecUsb.h>

#include <pid.hpp>

//}

namespace mrs_uav_asctec_api
{

/* class MrsUavAsctecApi //{ */

class MrsUavAsctecApi : public mrs_uav_hw_api::MrsUavHwApi {

public:
  ~MrsUavAsctecApi(){};

  void initialize(const ros::NodeHandle& parent_nh, std::shared_ptr<mrs_uav_hw_api::CommonHandlers_t> common_handlers);

  // | --------------------- status methods --------------------- |

  mrs_msgs::HwApiStatus       getStatus();
  mrs_msgs::HwApiCapabilities getCapabilities();

  // | --------------------- topic callbacks -------------------- |

  bool callbackActuatorCmd(mrs_lib::SubscribeHandler<mrs_msgs::HwApiActuatorCmd>& wrp);
  bool callbackControlGroupCmd(mrs_lib::SubscribeHandler<mrs_msgs::HwApiControlGroupCmd>& wrp);
  bool callbackAttitudeRateCmd(mrs_lib::SubscribeHandler<mrs_msgs::HwApiAttitudeRateCmd>& wrp);
  bool callbackAttitudeCmd(mrs_lib::SubscribeHandler<mrs_msgs::HwApiAttitudeCmd>& wrp);
  bool callbackAccelerationHdgRateCmd(mrs_lib::SubscribeHandler<mrs_msgs::HwApiAccelerationHdgRateCmd>& wrp);
  bool callbackAccelerationHdgCmd(mrs_lib::SubscribeHandler<mrs_msgs::HwApiAccelerationHdgCmd>& wrp);
  bool callbackVelocityHdgRateCmd(mrs_lib::SubscribeHandler<mrs_msgs::HwApiVelocityHdgRateCmd>& wrp);
  bool callbackVelocityHdgCmd(mrs_lib::SubscribeHandler<mrs_msgs::HwApiVelocityHdgCmd>& wrp);
  bool callbackPositionCmd(mrs_lib::SubscribeHandler<mrs_msgs::HwApiPositionCmd>& wrp);

  void callbackTrackerCmd(mrs_lib::SubscribeHandler<mrs_msgs::TrackerCommand>& wrp);

  // | -------------------- service callbacks ------------------- |

  std::tuple<bool, std::string> callbackArming(const bool& request);
  std::tuple<bool, std::string> callbackOffboard(void);

private:
  struct SAsctecSettings
  {
    std::string device;
    bool        vicon;
    bool        gps;
    bool        rc_serial_control;
  };

  bool is_initialized_ = false;

  std::shared_ptr<mrs_uav_hw_api::CommonHandlers_t> common_handlers_;

  // | ----------------------- parameters ----------------------- |

  mrs_msgs::HwApiCapabilities _capabilities_;

  std::string _uav_name_;
  std::string _body_frame_name_;
  std::string _world_frame_name_;

  bool _simulation_;

  // | ----------------------- subscribers ---------------------- |

  mrs_lib::SubscribeHandler<geometry_msgs::PoseStamped> sh_vicon_localization_;

  // | ----------------------- callbacks ---------------------- |

  static void callbackImu(CAttitude& attitude, void* uInput);
  static void callbackRC(SControlValues& rcValues, void* uInput);
  static void callbackState(SState& state, void* uInput);
  void        callbackVicon(mrs_lib::SubscribeHandler<geometry_msgs::PoseStamped>& wrp);

  // TODO:
  // void callbackMavrosState(mrs_lib::SubscribeHandler<mavros_msgs::State>& wrp);

  // | ------------------------ variables ----------------------- |

  std::atomic<bool> offboard_ = false;
  std::string       mode_;
  std::atomic<bool> armed_     = false;
  std::atomic<bool> connected_ = false;
  std::mutex        mutex_status_;

  double      _utm_x_;
  double      _utm_y_;
  std::string _utm_zone_;
  double      _amsl_;

  CAsctecUSB      _asctec_;
  SAsctecSettings _asctecSettings_;

  PIDController pid_yaw_;

  double     current_yaw_ = 0;
  std::mutex mutex_yaw_;

  bool      yaw_loop_first_iteration_ = true;
  ros::Time yaw_loop_last_time_;

  std::optional<geometry_msgs::PoseStamped> vicon_pose_;
};

//}

}  // namespace mrs_uav_asctec_api

#endif
