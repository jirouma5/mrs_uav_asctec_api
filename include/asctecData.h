/*
 * File name: asctec_data.h
 * Date:      2018/08/15 10:24
 * Author:    Jan Chudoba
 */

#ifndef __ASCTEC_DATA_H__
#define __ASCTEC_DATA_H__

#include <stdint.h>
#include <math.h>

typedef enum {
	ASCTEC_CONTROL_NONE,
	ASCTEC_CONTROL_MOTORS,
	ASCTEC_CONTROL_DIRECT,
	ASCTEC_CONTROL_HEIGHT,
	ASCTEC_CONTROL_POSITION
} TAsctecControlMode;

class CGpsDataRaw
{
public:
	int32_t latitude; // [1e-7 deg]
	int32_t longitude; // [1e-7 deg]
	int32_t altitude; // [1e-3 m]
	int32_t heading; // [1e-3 deg]
	uint32_t satNum;
	int32_t status; // it seems (contrary to original notes in sdk sources), that status is composed from ublox NAV-STATUS gpsFix | (flags << 8) | (fixStat << 16)

	double getLatitude() { return 1e-7 * (double) latitude; }
	double getLongitude() { return 1e-7 * (double) longitude; }
	double getAltitude() { return 1e-3 * (double) altitude; }
	double getAzimuth() { return 0.5*M_PI - (1e-3 * M_PI / 180.0) * (double) heading; } // result in [rad], 0=east, PI/2=north
};

class CGpsData
{
public:
	double latitude;
	double longitude;
	double altitude;
	double heading; // [rad], zero=east
	int satNum;
	int status; // it seems (contrary to original notes in sdk sources), that status is composed from ublox NAV-STATUS gpsFix | (flags << 8) | (fixStat << 16)
};

class CSimGpsData
{
public:
	int8_t mode;
	int8_t update;
	int32_t latitude;
	int32_t longitude;
	int32_t altitude; // [1e-3 * m]
	int32_t heading; // [1e-3 * deg]
};

class CAttitudeRaw
{
public:
	int32_t anglePitch;
	int32_t angleRoll;
	int32_t angleYaw;
};

class CAttitude
{
public:
	double anglePitch;
	double angleRoll;
	double angleYaw;
};

#endif

/* end of asctec_data.h */
