/* includes //{ */

#include <api.h>
#include <mrs_lib/gps_conversions.h>

//}


/* defines //{ */

#define VICON_DT 0.01

#define PWM_MIDDLE 1500
#define PWM_MIN 1000
#define PWM_MAX 2000
#define PWM_DEADBAND 200
#define PWM_RANGE PWM_MAX - PWM_MIN

//}

namespace mrs_uav_asctec_api
{

// --------------------------------------------------------------
// |                   controller's interface                   |
// --------------------------------------------------------------

/* initialize() //{ */

void MrsUavAsctecApi::initialize(const ros::NodeHandle& parent_nh, std::shared_ptr<mrs_uav_hw_api::CommonHandlers_t> common_handlers) {

  ros::NodeHandle nh_(parent_nh);

  common_handlers_ = common_handlers;

  _uav_name_         = common_handlers->getUavName();
  _body_frame_name_  = common_handlers->getBodyFrameName();
  _world_frame_name_ = common_handlers->getWorldFrameName();

  _capabilities_.api_name = "AsctecApi";

  // | ------------------- loading parameters ------------------- |

  mrs_lib::ParamLoader param_loader(nh_, "MrsUavHwApi");

  param_loader.loadParam("simulation", _simulation_);

  param_loader.loadParam("inputs/control_group", (bool&)_capabilities_.accepts_control_group_cmd);
  param_loader.loadParam("inputs/attitude_rate", (bool&)_capabilities_.accepts_attitude_rate_cmd);
  param_loader.loadParam("inputs/attitude", (bool&)_capabilities_.accepts_attitude_cmd);

  param_loader.loadParam("outputs/distance_sensor", (bool&)_capabilities_.produces_distance_sensor);
  param_loader.loadParam("outputs/gnss", (bool&)_capabilities_.produces_gnss);
  param_loader.loadParam("outputs/rtk", (bool&)_capabilities_.produces_rtk);
  param_loader.loadParam("outputs/ground_truth", (bool&)_capabilities_.produces_ground_truth);
  param_loader.loadParam("outputs/imu", (bool&)_capabilities_.produces_imu);
  param_loader.loadParam("outputs/altitude", (bool&)_capabilities_.produces_altitude);
  param_loader.loadParam("outputs/magnetometer_heading", (bool&)_capabilities_.produces_magnetometer_heading);
  param_loader.loadParam("outputs/rc_channels", (bool&)_capabilities_.produces_rc_channels);
  param_loader.loadParam("outputs/battery_state", (bool&)_capabilities_.produces_battery_state);
  param_loader.loadParam("outputs/position", (bool&)_capabilities_.produces_position);
  param_loader.loadParam("outputs/orientation", (bool&)_capabilities_.produces_orientation);
  param_loader.loadParam("outputs/velocity", (bool&)_capabilities_.produces_velocity);
  param_loader.loadParam("outputs/angular_velocity", (bool&)_capabilities_.produces_angular_velocity);
  param_loader.loadParam("outputs/odometry", (bool&)_capabilities_.produces_odometry);

  param_loader.loadParam("asctec/device", _asctecSettings_.device);
  param_loader.loadParam("asctec/vicon", (bool&)_asctecSettings_.vicon);
  param_loader.loadParam("asctec/gps", (bool&)_asctecSettings_.gps);
  param_loader.loadParam("asctec/rc_serial_control", (bool&)_asctecSettings_.rc_serial_control);

  param_loader.loadParam("gnss/utm_x", _utm_x_);
  param_loader.loadParam("gnss/utm_y", _utm_y_);
  param_loader.loadParam("gnss/utm_zone", _utm_zone_);
  param_loader.loadParam("gnss/amsl", _amsl_);

  if (!param_loader.loadedSuccessfully()) {
    ROS_ERROR("[MrsUavAsctecApi]: Could not load all parameters!");
    ros::shutdown();
  }

  // | ----------------------- subscribers ---------------------- |

  mrs_lib::SubscribeHandlerOptions shopts;
  shopts.nh                 = nh_;
  shopts.node_name          = "MrsHwAsctecApi";
  shopts.no_message_timeout = mrs_lib::no_timeout;
  shopts.threadsafe         = true;
  shopts.autostart          = true;
  shopts.queue_size         = 10;
  shopts.transport_hints    = ros::TransportHints().tcpNoDelay();

  // | ----------------------- init the HW ---------------------- |
  // init the device
  if (!_asctec_.open(_asctecSettings_.device.c_str())) {
    ROS_ERROR("Failed to open device %s", _asctecSettings_.device.c_str());
  } else {
    ROS_INFO("Device %s opened successfully", _asctecSettings_.device.c_str());
  }

  // set callbacks
  _asctec_.setIMUCallback(callbackImu, (void*)this);
  _asctec_.setRCCallback(callbackRC, (void*)this);
  _asctec_.setStateCallback(callbackState, (void*)this);

  ROS_INFO("callbacks assigned");

  // disable gps
  if (!_asctecSettings_.gps) {
    _asctec_.disableGps();
    ROS_INFO("gps disabled");
  }

  if (!_asctec_.initialize()) {
    ROS_ERROR("Failed to initialize communication");
  }

  ROS_INFO("asctec initialized");

  // create vicon subscriber
  if (_asctecSettings_.vicon) {
    sh_vicon_localization_ = mrs_lib::SubscribeHandler<geometry_msgs::PoseStamped>(shopts, "vicon_localization_in", &MrsUavAsctecApi::callbackVicon, this);
  }

  // only for thrust experiment
  // _asctec_.setControlMode(ASCTEC_CONTROL_HEIGHT);
  // _asctec_.enableCtrl();

  // | --------------------------- pid -------------------------- |

  pid_yaw_.setParams(0.5, 0.03, 0.01, 1.0, -1.0);

  // | ----------------------- finish init ---------------------- |

  ROS_INFO("[MrsUavAsctecApi]: initialized");

  is_initialized_ = true;
}

//}

/* getStatus() //{ */

mrs_msgs::HwApiStatus MrsUavAsctecApi::getStatus() {

  mrs_msgs::HwApiStatus status;

  status.stamp = ros::Time::now();

  {
    std::scoped_lock lock(mutex_status_);

    status.armed     = armed_;
    status.offboard  = offboard_;
    status.connected = connected_;
    status.mode      = mode_;
  }

  return status;
}

//}

/* getCapabilities() //{ */

mrs_msgs::HwApiCapabilities MrsUavAsctecApi::getCapabilities() {

  _capabilities_.stamp = ros::Time::now();

  return _capabilities_;
}

//}

/* callbackControlActuatorCmd() //{ */

bool MrsUavAsctecApi::callbackActuatorCmd([[maybe_unused]] mrs_lib::SubscribeHandler<mrs_msgs::HwApiActuatorCmd>& wrp) {

  ROS_INFO_ONCE("[MrsUavAsctecApi]: getting actuator cmd");

  return false;
}

//}

/* callbackControlGroupCmd() //{ */

bool MrsUavAsctecApi::callbackControlGroupCmd([[maybe_unused]] mrs_lib::SubscribeHandler<mrs_msgs::HwApiControlGroupCmd>& wrp) {

  ROS_INFO_ONCE("[MrsUavAsctecApi]: getting control group cmd");

  if (!_capabilities_.accepts_control_group_cmd) {
    ROS_ERROR("[MrsUavAsctecApi]: the control group input is not enabled in the config file");
    return false;
  }

  return true;
}

//}

/* callbackAttitudeRateCmd() //{ */

bool MrsUavAsctecApi::callbackAttitudeRateCmd([[maybe_unused]] mrs_lib::SubscribeHandler<mrs_msgs::HwApiAttitudeRateCmd>& wrp) {

  ROS_INFO_ONCE("[MrsUavAsctecApi]: getting attitude rate cmd");

  if (!_capabilities_.accepts_attitude_rate_cmd) {
    ROS_ERROR_THROTTLE(1.0, "[MrsUavAsctecApi]: attitude rate input is not enabled in the config file");
    return false;
  }

  return true;
}

//}

/* callbackAttitudeCmd() //{ */

bool MrsUavAsctecApi::callbackAttitudeCmd([[maybe_unused]] mrs_lib::SubscribeHandler<mrs_msgs::HwApiAttitudeCmd>& wrp) {

  ROS_INFO_ONCE("[MrsUavAsctecApi]: getting attitude cmd");

  if (!_capabilities_.accepts_attitude_cmd) {
    ROS_ERROR_THROTTLE(1.0, "[MrsUavAsctecApi]: attitude input is not enabled in the config file");
    return false;
  }

  auto msg = wrp.getMsg();

  auto current_yaw = mrs_lib::get_mutexed(mutex_yaw_, current_yaw_);

  mrs_lib::EulerAttitude att = mrs_lib::AttitudeConverter(msg->orientation);

  SControlValues attitudeTarget;
  // asctec api has opposite orientation of the pitch angle
  attitudeTarget.elevator = -att.pitch();
  attitudeTarget.aileron  = att.roll();
  attitudeTarget.thrust   = msg->throttle;

  // TODO: asctec interface accepts heading-rate and pitch, roll
  //  temporal solution- set heading-rate to 0
  // attitudeTarget.rudder = att.yaw();

  // | ----------------------- yaw control ---------------------- |

  double dt = 0.01;

  if (yaw_loop_first_iteration_) {

    yaw_loop_last_time_       = ros::Time::now();
    yaw_loop_first_iteration_ = false;

  } else {
    dt                  = (ros::Time::now() - yaw_loop_last_time_).toSec();
    yaw_loop_last_time_ = ros::Time::now();

    if (dt < 0.005) {
      ROS_WARN_THROTTLE(1.0, "[MrsUavAsctecApi]: calculate dt < 0.005, setting it to 0.01");
      dt = 0.01;
    } else if (dt > 0.1) {
      ROS_WARN_THROTTLE(1.0, "[MrsUavAsctecApi]: calculate dt > 0.1, setting it to 0.01");
      dt = 0.01;
    }
  }

  const double desired_yaw = mrs_lib::AttitudeConverter(msg->orientation).getYaw();

  double yaw_error        = desired_yaw - current_yaw;
  double yaw_rate_action_ = pid_yaw_.update(yaw_error, dt);

  // asctec api has opposite orientation of the yaw angle
  attitudeTarget.rudder = -yaw_rate_action_;

  _asctec_.setControlAttitude(attitudeTarget);

  return true;
}

//}

/* callbackAccelerationHdgRateCmd() //{ */

bool MrsUavAsctecApi::callbackAccelerationHdgRateCmd([[maybe_unused]] mrs_lib::SubscribeHandler<mrs_msgs::HwApiAccelerationHdgRateCmd>& wrp) {

  ROS_INFO_ONCE("[MrsUavAsctecApi]: getting acceleration+hdg rate cmd");

  // TODO?

  return false;
}

//}

/* callbackAccelerationHdgCmd() //{ */

bool MrsUavAsctecApi::callbackAccelerationHdgCmd([[maybe_unused]] mrs_lib::SubscribeHandler<mrs_msgs::HwApiAccelerationHdgCmd>& wrp) {

  ROS_INFO_ONCE("[MrsUavAsctecApi]: getting acceleration+hdg cmd");

  return false;
}

//}

/* callbackVelocityHdgRateCmd() //{ */

bool MrsUavAsctecApi::callbackVelocityHdgRateCmd([[maybe_unused]] mrs_lib::SubscribeHandler<mrs_msgs::HwApiVelocityHdgRateCmd>& wrp) {

  ROS_INFO_ONCE("[MrsUavAsctecApi]: getting velocity+hdg rate cmd");

  return false;
}

//}

/* callbackVelocityHdgCmd() //{ */

bool MrsUavAsctecApi::callbackVelocityHdgCmd([[maybe_unused]] mrs_lib::SubscribeHandler<mrs_msgs::HwApiVelocityHdgCmd>& wrp) {

  ROS_INFO_ONCE("[MrsUavAsctecApi]: getting velocity+hdg  cmd");

  return false;
}

//}

/* callbackPositionCmd() //{ */

bool MrsUavAsctecApi::callbackPositionCmd([[maybe_unused]] mrs_lib::SubscribeHandler<mrs_msgs::HwApiPositionCmd>& wrp) {

  ROS_INFO_ONCE("[MrsUavAsctecApi]: getting position cmd");

  return false;
}

//}

/* callbackTrackerCmd() //{ */

void MrsUavAsctecApi::callbackTrackerCmd([[maybe_unused]] mrs_lib::SubscribeHandler<mrs_msgs::TrackerCommand>& wrp) {
}

//}

/* callbackArming() //{ */

std::tuple<bool, std::string> MrsUavAsctecApi::callbackArming([[maybe_unused]] const bool& request) {

  // TODO: make it functional

  std::stringstream ss;

  if (request) {
    ss << "Arming is not allowed using the companion computer.";
    ROS_WARN_STREAM_THROTTLE(1.0, "[AsctecApi]: " << ss.str());
    return std::tuple(false, ss.str());
  }

  if (!request && !offboard_) {
    ss << "can not disarm, not in OFFBOARD mode";
    ROS_WARN_STREAM_THROTTLE(1.0, "[AsctecApi]: " << ss.str());
    return std::tuple(false, ss.str());
  }

  ROS_INFO("[AsctecApi]: calling for %s", request ? "arming" : "disarming");

  bool result;

  if (request) {
    result = _asctec_.arm();
  } else {
    result = _asctec_.disarm();
  }

  if (result) {
    ss << "service call for " << (request ? "arming" : "disarming") << " was successful";
    ROS_INFO_STREAM_THROTTLE(1.0, "[AsctecApi]: " << ss.str());

  } else {
    ss << "service call for " << (request ? "arming" : "disarming") << " failed";
    ROS_ERROR_STREAM_THROTTLE(1.0, "[AsctecApi]: " << ss.str());
  }

  return {result, ss.str()};
}
//}

/* callbackOffboard() //{ */

std::tuple<bool, std::string> MrsUavAsctecApi::callbackOffboard(void) {

  _asctec_.setControlMode(ASCTEC_CONTROL_HEIGHT);
  _asctec_.enableCtrl();

  std::stringstream ss;

  ss << "switched to offboard mode";

  return {true, ss.str()};
}

//}

// | ------------------------ callbacks ----------------------- |

/* //{ callbackState() */

void MrsUavAsctecApi::callbackState(SState& state, void* uInput) {

  MrsUavAsctecApi* api = (MrsUavAsctecApi*)uInput;

  if (!(api->is_initialized_)) {
    return;
  }

  ROS_INFO_ONCE("[MrsUavAsctecApi]: getting Astec state");

  mrs_msgs::HwApiStatus status;

  {
    std::scoped_lock lock(api->mutex_status_);

    status.stamp = ros::Time::now();

    // TODO: implement better way of detecting arming

    status.armed    = state.armed;
    status.offboard = state.offboard;

    // TODO:
    status.connected = api->_asctec_.uavSerialControlEnabled();

    if (state.status & UAV_STATUS_MASK_POSITION_CONTROL) {
      status.mode = "POSITION_CONTROL";
    } else if (state.status & UAV_STATUS_MASK_HEIGHT_CONTROL) {
      status.mode = "HEIGHT_CONTROL";
    } else if (state.status & UAV_STATUS_MASK_ALTITUDE_CONTROL) {
      status.mode = "ATTITUDE_CONTROL";
    } else if (state.status & UAV_STATUS_MASK_FLYING) {
      status.mode = "FLYING";
    }

    api->armed_ = state.armed;
    api->offboard_ = state.offboard; 
    api->connected_ = api->_asctec_.uavSerialControlEnabled();;
    api->mode_ = status.mode;
  }

  api->common_handlers_->publishers.publishStatus(status);
}

//}

/* callbackImu() //{ */

void MrsUavAsctecApi::callbackImu(CAttitude& attitude, void* uInput) {

  MrsUavAsctecApi* api = (MrsUavAsctecApi*)uInput;

  // TODO: asctec gives only attitude estimate (angles) - no velocity and no acceleration

  if (!(api->is_initialized_)) {
    return;
  }

  ROS_INFO_ONCE("[MrsUavAsctecApi]: getting IMU");

  // Thrust experiment
  // SControlValues controlAction;
  // controlAction.elevator = 0.0;
  // controlAction.aileron = 0.0;
  // controlAction.rudder = 0.0;
  // controlAction.thrust = 1.0;
  // api->_asctec_.setControlAttitude(controlAction);

  if (api->_capabilities_.produces_imu) {

    // map asctecs orientation convention to right-hand-rule
  	//	orientation convention

    ROS_DEBUG("roll: %.3f, pitch: %.3f, yaw: %.3f",
      attitude.angleRoll, -attitude.anglePitch, attitude.angleYaw);

    sensor_msgs::Imu new_imu_msg;
    new_imu_msg.header.frame_id = api->_uav_name_ + "/" + api->_body_frame_name_;
    new_imu_msg.orientation     = mrs_lib::AttitudeConverter(attitude.angleRoll, -attitude.anglePitch, attitude.angleYaw);

    api->common_handlers_->publishers.publishIMU(new_imu_msg);
  }
}

//}

/* callbackRC() //{ */

void MrsUavAsctecApi::callbackRC(SControlValues& rcValues, void* uInput) {

  MrsUavAsctecApi* api = (MrsUavAsctecApi*)uInput;

  if (!(api->is_initialized_)) {
    return;
  }

  ROS_INFO_ONCE("[MrsUavAsctecApi]: getting RC");

  // ROS_INFO("elevator: %.3f, aileron: %.3f, rudder: %.3f, thrust: %.3f",
  //   rcValues.elevator, rcValues.aileron, rcValues.rudder, rcValues.thrust);

  if (api->_capabilities_.produces_rc_channels) {

    mrs_msgs::HwApiRcChannels rc_out;

    rc_out.stamp = ros::Time::now();

    rc_out.channels.push_back(rcValues.elevator);
    rc_out.channels.push_back(rcValues.aileron);
    rc_out.channels.push_back(rcValues.rudder);
    rc_out.channels.push_back(rcValues.thrust);

    api->common_handlers_->publishers.publishRcChannels(rc_out);
  }
}

//}

/* callbackVicon() //{ */

void MrsUavAsctecApi::callbackVicon(mrs_lib::SubscribeHandler<geometry_msgs::PoseStamped>& wrp) {

  if (!is_initialized_) {
    return;
  }

  ROS_INFO_ONCE("[MrsUavAsctecApi]: getting vicon position");

  geometry_msgs::PoseStampedConstPtr msg = wrp.getMsg();

  // | -------------------- publish position -------------------- |

  geometry_msgs::PointStamped position;

  position.header.stamp    = msg->header.stamp;
  position.header.frame_id = _uav_name_ + "/" + _world_frame_name_;
  position.point           = msg->pose.position;

  common_handlers_->publishers.publishPosition(position);

  // | -------------------- publish velocity -------------------- |

  double vel_x;
  double vel_y;
  double vel_z;

  if (!vicon_pose_) {

    vicon_pose_ = *msg;

  } else {

    double dt = (msg->header.stamp - vicon_pose_->header.stamp).toSec();

    // TODO: use VICON_DT (0.01) instead of meassured dt?
    // if (dt < 0 || dt > 0.1) {
    //   ROS_ERROR("[MrsUavAsctecApi]: vicon dt is strange, not differentiating velocity: %.3f", dt);
    // } else {

      // TODO: use VICON_DT (0.01) instead of meassured dt?
      vel_x = (msg->pose.position.x - vicon_pose_->pose.position.x) / VICON_DT;
      vel_y = (msg->pose.position.y - vicon_pose_->pose.position.y) / VICON_DT;
      vel_z = (msg->pose.position.z - vicon_pose_->pose.position.z) / VICON_DT;

      Eigen::Matrix3d R = mrs_lib::AttitudeConverter(msg->pose.orientation);

      Eigen::Vector3d vel_world(vel_x, vel_y, vel_z);
      Eigen::Vector3d vel_body = R.transpose() * vel_world;

      geometry_msgs::Vector3Stamped vel;

      vel.header.frame_id = _uav_name_ + "/" + _body_frame_name_;
      vel.header.stamp    = msg->header.stamp;

      vel.vector.x = vel_body[0];
      vel.vector.y = vel_body[1];
      vel.vector.z = vel_body[2];

      common_handlers_->publishers.publishVelocity(vel);
    // }

    vicon_pose_ = *msg;
  }

  // | ------------------- publish orientation ------------------ |

  geometry_msgs::QuaternionStamped orientation;

  orientation.header.stamp    = msg->header.stamp;
  orientation.header.frame_id = _uav_name_ + "/" + _world_frame_name_;

  // auto [r, p, y] = mrs_lib::AttitudeConverter(msg->pose.orientation).getExtrinsicRPY();
  // ROS_DEBUG("vicon callback roll: %.3f, pitch: %.3f, yaw: %.3f", r, p, y);
  
  orientation.quaternion = msg->pose.orientation;

  common_handlers_->publishers.publishOrientation(orientation);

  // save current yaw
  {
    std::scoped_lock lock(mutex_yaw_);

    try {
      current_yaw_ = mrs_lib::AttitudeConverter(msg->pose.orientation).getYaw();
    }
    catch (...) {
      ROS_ERROR("exception caught while getting yaw");
    }
  }

  // | ---------------------- publish gnss ---------------------- |

  double lat;
  double lon;

  mrs_lib::UTMtoLL(msg->pose.position.y + _utm_y_, msg->pose.position.x + _utm_x_, _utm_zone_, lat, lon);

  sensor_msgs::NavSatFix gnss;

  gnss.header.stamp = msg->header.stamp;

  gnss.latitude  = lat;
  gnss.longitude = lon;
  gnss.altitude  = msg->pose.position.z + _amsl_;

  common_handlers_->publishers.publishGNSS(gnss);

  // publish ground truth

	nav_msgs::Odometry gt;

  gt.header.stamp = ros::Time::now();
  gt.header.frame_id = _uav_name_ + "/" + _world_frame_name_;
  gt.child_frame_id = _uav_name_ + "/" + _body_frame_name_;
  gt.pose.pose.position = msg->pose.position;
  gt.pose.pose.orientation = msg->pose.orientation;
  gt.twist.twist.linear.x = vel_x;
  gt.twist.twist.linear.y = vel_y;
  gt.twist.twist.linear.z = vel_z;
	 
  common_handlers_->publishers.publishGroundTruth(gt);

  // | --------------------- publish heading -------------------- |

  double heading = mrs_lib::AttitudeConverter(msg->pose.orientation).getHeading();

  mrs_msgs::Float64Stamped hdg;

  hdg.header.stamp = msg->header.stamp;
  hdg.value        = heading;

  common_handlers_->publishers.publishMagnetometerHeading(hdg);

  // | ---------------------- publish amsl ---------------------- |

  mrs_msgs::HwApiAltitude altitude;

  altitude.stamp = msg->header.stamp;

  altitude.amsl = msg->pose.position.z + _amsl_;

  common_handlers_->publishers.publishAltitude(altitude);

  // | ---------------- publish angular velocity ---------------- |

  geometry_msgs::Vector3Stamped angular_vel;

  angular_vel.header.frame_id = _uav_name_ + "/" + _body_frame_name_;
  angular_vel.header.stamp    = msg->header.stamp;

  angular_vel.vector.x = 0;
  angular_vel.vector.y = 0;
  angular_vel.vector.z = 0;

  common_handlers_->publishers.publishAngularVelocity(angular_vel);
}

//}

}  // namespace mrs_uav_asctec_api

#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(mrs_uav_asctec_api::MrsUavAsctecApi, mrs_uav_hw_api::MrsUavHwApi)
