#include "utilities.h"

double limitValue(double value, double maxValue){
  return limitValue2(value, -maxValue, maxValue);
}

double limitValue2(double value, double minValue, double maxValue){
	if (value < minValue) value = minValue;
	if (value > maxValue) value = maxValue;
	return value;
}