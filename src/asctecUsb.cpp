/*
 * File name: asctec_usb.cc
 * Date:      2017/12/14 10:19
 * Author:    Jan Chudoba
 */

#include <unistd.h> // UNIX standard function definitions
#include <fcntl.h>  // File control definitions
#include <termios.h> // POSIX terminal control definitionss
#include <time.h>
#include <string.h>
#include <errno.h>
#include <math.h>
#include <sys/time.h>

#include <asctecUsb.h>
#include <asctecCommIntf.h>

#define INTERNAL_ENGINE_RATE_HZ 1000 // this is fixed in firmware
#define ENGINE_RATE_HZ 100
#define DEFAULT_VARIABLE_REQUEST_PERIOD_MS 20

CAsctecUSB * CAsctecUSB::instance = NULL;

enum TCmdPacketId
{
	CMDPACKET_DEFAULT = 0,
	CMDPACKET_WAYPOINT = 1,
	CMDPACKET_SIMGPS = 2
};

CAsctecUSB::CAsctecUSB() :
	IMUCallback(NULL),
	RCCallback(NULL),
    StateCallback(NULL),
	fd(-1),
	threadStarted(false),
	commandsReady(false),
	variablesReady(false),
	parametersReady(false),
	internalDataUpdated(false),
	externalDataUpdated(false),
	variableRequestPeriodMs(DEFAULT_VARIABLE_REQUEST_PERIOD_MS),
	readMotorRpm(true),
	noGpsMode(false),
	armed(false),
	ctrlMode(2),
	ctrlEnabled(0),
	waypointIndexGenerator(0),
	debugLevel(0),
	fEngineLog(NULL)
{
	if (instance == NULL) {
		instance = this;
	}

	fEngineLog = fopen("engine.log", "w");

	aciInit();
	aciSetSendDataCallback(&transmit);

	gpsPosition.latitude = -1;
	gpsPosition.longitude = -1;
	gpsPosition.altitude = -1;
	gpsPosition.heading = -1;
	gpsPosition.satNum = 1024;
	gpsPosition.status = -1;
	gpsTimeOfWeek = 0;
	for (unsigned int i=0; i < (sizeof(rcChannel)/sizeof(rcChannel[0])); i++) rcChannel[i] = 0;
	for (unsigned int i=0; i < (sizeof(motorRPM)/sizeof(motorRPM[0])); i++) motorRPM[i] = 0;
}

CAsctecUSB::~CAsctecUSB()
{
	if (fEngineLog) {
		fclose(fEngineLog);
	}
	close();
	if (instance == this) {
		instance = NULL;
	}
	aciFree();
}

bool CAsctecUSB::open(const char * device)
{
	if (fd >= 0 || threadStarted) close();

	fd = ::open(device, O_RDWR | O_NOCTTY | O_NDELAY | O_NONBLOCK);
	if (fd < 0) {
		//if (debugLevel > 0) fprintf(stderr, "CAsctecUSB::open: Failed to open device %s: %s\n", device, strerror(errno));
		ROS_DEBUG("CAsctecUSB::open: Failed to open device %s: %s", device, strerror(errno));
		return false;
	}

	struct termios port_settings; // structure to store the port settings in
	memset(&port_settings, 0, sizeof(port_settings));
	cfsetispeed(&port_settings, B57600); // set baud rates
	port_settings.c_cflag = B57600 | CS8 | CREAD | CLOCAL;
	port_settings.c_iflag = IGNPAR;
	port_settings.c_oflag = 0;
	port_settings.c_lflag = 0;
	tcsetattr(fd, TCSANOW, &port_settings); // apply the settings to the port

	threadInterrupt = false;
	threadStarted = (pthread_create(&thread, NULL, thread_body, this) == 0);
	return threadStarted;
}

void CAsctecUSB::close()
{
	threadInterrupt = true;
	if (fd >= 0) {
		::close(fd);
		fd = -1;
	}
	if (threadStarted) {
		pthread_join(thread, 0);
		threadStarted = false;
	}
}

static uint32_t getTimeMs()
{
	uint32_t t;
	struct timeval now;
	gettimeofday(&now, NULL);
	t = now.tv_sec * 1000 + now.tv_usec / 1000;
	return t;
}

bool CAsctecUSB::initialize(int rate)
{
	if (rate > 0) {
		if (rate > 100) rate = 100;
		variableRequestPeriodMs = rate;
		if (debugLevel > 1) fprintf(stderr, "setting variable rate to %u Hz\n", variableRequestPeriodMs);
	}
	aciInfoPacketReceivedCallback(&versionsUpdated);
	aciSetVarListUpdateFinishedCallback(&varListUpdateFinished);
	aciSetCmdListUpdateFinishedCallback(&cmdListUpdateFinished);
	aciSetParamListUpdateFinishedCallback(&paramListUpdateFinished);
	aciVarPacketReceivedCallback(&varPacketReceived);
	aciSetEngineRate(ENGINE_RATE_HZ, 100); // (calls/s, heartbeat)
	// heartbeat rate [Hz] = ENGINE_RATE_HZ / heartbeat
	if (fEngineLog) {
		fprintf(fEngineLog, "%u aciSetEngineRate(%u, %u)\n", getTimeMs(), ENGINE_RATE_HZ, 10);
	}
	aciCheckVerConf();
	aciGetVarPacketRateFromDevice();
	aciSendParamLoad();

	usleep(100000);
	if (aciGetInfo().verMajor != 0) {
		if (debugLevel > 1)	ROS_DEBUG("ACI version info: %d.%d\n", aciGetInfo().verMajor, aciGetInfo().verMinor);
	} else {
		ROS_DEBUG("ACI version info not received");
	}

	ROS_INFO("getting command list");
	aciGetDeviceCommandsList();

	int timeout = 5000;
	while (!commandsReady) {
		if (timeout-- > 0) {
			usleep(1000);
		} else {
			ROS_DEBUG("aci_usb commands timeout");
			return false;
		}
	}

	usleep(500000);

	ROS_INFO("getting var list");
	aciGetDeviceVariablesList();

	timeout = 4000;
	while (!variablesReady) {
		if (timeout-- > 0) {
			usleep(1000);
		} else {
			ROS_DEBUG("aci_usb variables timeout");
			return false;
		}
	}

	usleep(500000);
	aciGetDeviceParametersList();

	timeout = 6000;
	while (!parametersReady) {
		if (timeout-- > 0) {
			usleep(1000);
		} else {
			if (debugLevel > 0) fprintf(stderr, "aci_usb parameters timeout\n");
			if (debugLevel > 0) fprintf(stderr, "WARNING: aci_usb parameters timeout IGNORED !!!\n");
			break;
			//return false;
		}
	}
	if (fEngineLog) fprintf(fEngineLog, "%u parameters configured\n", getTimeMs());

	return true;
}

void CAsctecUSB::threadBody()
{
	ROS_DEBUG("ACI thread started");
	int counter = 1;
	int byteCounter = 0;
	int noByteCounter = 0;
	uint32_t t0 = 0;
	uint32_t dataRateTime = getTimeMs();
	unsigned int dataRateCounter = 0;
	uint32_t tEngine = 0;
	uint16_t uavStatusPrev = 0;
	//FILE * fByteDebugLog = fopen("bytes.log", "w");
	//fprintf(stderr, "fd: %d\n", fd);
	while (!threadInterrupt) {
		unsigned char data = 0;
		//fprintf(stderr, "fd: %d\n", fd);
		int result = read(fd, &data, 1); // read is non-blocking
		if (data){
		//fprintf(stderr, "data: %d\n", data);
		}
#if 0
		if ((result == 1) && (byteCounter < 1024) && (!internalDataUpdated) ) { // TODO: check if it is ok to skip to processing if data packet was received ...
			aciReceiveHandler(data);
			byteCounter++;
		} else
#else
		if (result == 1) {
			//if (fByteDebugLog) fprintf(fByteDebugLog, "%u\n", getTimeMs());
			aciReceiveHandler(data);
			byteCounter++;
		} else {
			noByteCounter++;
		}
		//fprintf(stderr, "result: %d\n", result);
		//fprintf(stderr, "byte counter: %d\n", byteCounter);
		//fprintf(stderr, "no counter: %d\n", noByteCounter);
		if (internalDataUpdated || (byteCounter > 256) || (noByteCounter > 256))
#endif
		{
			uint32_t t = getTimeMs();
			if (debugLevel > 2 && byteCounter > 0) fprintf(stderr, "read %d bytes\n", byteCounter);
			if (debugLevel > 1 && (byteCounter > 0)) {
				// calc data rate
				dataRateCounter += byteCounter;
				uint32_t dtDataRate = t - dataRateTime;
				if (dtDataRate > 1000) {
					unsigned int dataRate = dataRateCounter * 1000 / dtDataRate;
					dataRateCounter = 0;
					dataRateTime = t;
					fprintf(stderr, "data rate %u B/s (%u baud)\n", dataRate, 10*dataRate);
				}
			}
			byteCounter = 0;
			noByteCounter = 0;
			//fprintf(stderr, "variables ready: %d\n", variablesReady ? 1 : 0);
			//fprintf(stderr, "data updated: %d\n", internalDataUpdated ? 1 : 0);
			if (variablesReady && internalDataUpdated) {
				//fprintf(stderr, "while\n");
				// synchronize variables
				internalDataUpdated = false;
				aciSynchronizeVars();
				externalDataUpdated = true;
				// TODO: call callback if set

				// IMU callback
				if (IMUCallback){
					//fprintf(stderr, "calling IMU callback\n");

					CAttitude attitudeRad;
					attitudeRad.anglePitch = (double)attitude.anglePitch / 1000.0 / 180 * M_PI;
					attitudeRad.angleRoll = (double)attitude.angleRoll / 1000.0 / 180 * M_PI;
					attitudeRad.angleYaw = (double)attitude.angleYaw / 1000.0 / 180 * M_PI;
					IMUCallback(attitudeRad, IMUCallInput);
				}

				if (RCCallback){
					//fprintf(stderr, "calling RC callback\n");

					SControlValues rcValues;
					rcValues.elevator = RC_GAIN*limitValue((double)((int)rcChannel[0] - RC_CENTER_POS) / (double)RC_CENTER_POS, 1);
					rcValues.aileron = RC_GAIN*limitValue((double)((int)rcChannel[1] - RC_CENTER_POS) / (double)RC_CENTER_POS, 1);
					rcValues.rudder = limitValue((double)((int)rcChannel[3] - RC_RUDDER_CENTER_POS) / (double)RC_RUDDER_CENTER_POS, 1);
					rcValues.thrust = limitValue2((double)((int)rcChannel[2]) / (double)(2*RC_CENTER_POS), 0, 1);
					RCCallback(rcValues, RCCallInput);
				}

				if (StateCallback){
					//fprintf(stderr, "calling status callback\n");

					// check armed status based od average rpm of motors
					double avgMotorRPM = 0;
					for (int i = 0; i < 6; i++){
						avgMotorRPM += motorRPM[i];
					}
					avgMotorRPM /= 6;

					if (avgMotorRPM < 10){
						armed = false;
					} else {
						armed = true;
					}

					SState state;
					state.status = uavStatus;
					state.armed = armed;
					state.offboard = ctrlEnabled > 0;
					StateCallback(state, StateCallInput);
				}

				if ((debugLevel > 2) || (counter % 50 == 0)) {
					// ROS_DEBUG("status=%04X, bat=%5.2fV, RC=[%6u %6u %6u %6u   %6u %6u %6u %6u]", uavStatus, 1e-3*(double)uavBatteryVoltage, rcChannel[0], rcChannel[1], rcChannel[2], rcChannel[3], rcChannel[4], rcChannel[5], rcChannel[6], rcChannel[7]);
					// ROS_DEBUG("attitude: p %.3f, r %.3f, y %.3f", (double)attitude.anglePitch / 1000.0, (double) attitude.angleRoll / 1000.0, (double) attitude.angleYaw / 1000.0);
					// ROS_DEBUG("GPS   %8d, %8d, %3d, %d sats, t=%u status=%04X", gpsPosition.latitude, gpsPosition.longitude, gpsPosition.altitude, gpsPosition.satNum, gpsTimeOfWeek, gpsPosition.status);
					// ROS_DEBUG("fused %8d, %8d, %3d", fusedPosition.latitude, fusedPosition.longitude, fusedPosition.altitude);
					// ROS_DEBUG("rpm %3u %3u %3u %3u %3u %3u", motorRPM[0], motorRPM[1], motorRPM[2], motorRPM[3], motorRPM[4], motorRPM[5]);

					if (debugLevel > 1) fprintf(stderr, "status=%04X, bat=%5.2fV, RC=[%6u %6u %6u %6u   %6u %6u %6u %6u]\n", uavStatus, 1e-3*(double)uavBatteryVoltage, rcChannel[0], rcChannel[1], rcChannel[2], rcChannel[3], rcChannel[4], rcChannel[5], rcChannel[6], rcChannel[7]);
					if (debugLevel > 1) fprintf(stderr, "attitude: p %.3f, r %.3f, y %.3f\n", (double)attitude.anglePitch / 1000.0, (double) attitude.angleRoll / 1000.0, (double) attitude.angleYaw / 1000.0);
					if (debugLevel > 1) fprintf(stderr, "GPS   %8d, %8d, %3d, %d sats, t=%u status=%04X\n", gpsPosition.latitude, gpsPosition.longitude, gpsPosition.altitude, gpsPosition.satNum, gpsTimeOfWeek, gpsPosition.status);
					if (debugLevel > 1) fprintf(stderr, "fused %8d, %8d, %3d\n", fusedPosition.latitude, fusedPosition.longitude, fusedPosition.altitude);
					if (readMotorRpm) fprintf(stderr, "rpm %3u %3u %3u %3u %3u %3u\n", motorRPM[0], motorRPM[1], motorRPM[2], motorRPM[3], motorRPM[4], motorRPM[5]);
					uint32_t t = getTimeMs();
					if (t0 != 0) {
						//if (debugLevel > 1) fprintf(stderr, "period %u ms\n", (t - t0) / 50);
					}
					t0 = t;
				}
			}

			if (uavStatusPrev != uavStatus) {
				uavStatusPrev = uavStatus;
				if (fEngineLog) {
					fprintf(fEngineLog, "%u uav status = %04X (serial = %u)\n", getTimeMs(), uavStatus, (uavStatus & UAV_STATUS_MASK_SERIAL_ENABLED) ? 1 : 0);
				}
			}

			uint32_t dtEngine = t - tEngine;
			if (dtEngine >= (1000/ENGINE_RATE_HZ)) {
				tEngine = t;
				aciEngine();

				if (fEngineLog) {
					fprintf(fEngineLog, "%u aciEngine dt=%u rate=%u\n", getTimeMs(), dtEngine, ENGINE_RATE_HZ);
				}
				static unsigned int gLastDbgHeartbeatCounter = 0;
				static uint32_t lastHeartbeatTime = 0;
				if (gDbgHeartBeatCounter != gLastDbgHeartbeatCounter) {
					unsigned int dif = gDbgHeartBeatCounter - gLastDbgHeartbeatCounter;
					uint32_t t = getTimeMs();
					uint32_t dt = t - lastHeartbeatTime;
					lastHeartbeatTime = t;
					gLastDbgHeartbeatCounter = gDbgHeartBeatCounter;
					if (fEngineLog) {
						fprintf(fEngineLog, "%u aciEngine dt=%u rate=%u\n", getTimeMs(), dtEngine, ENGINE_RATE_HZ);
						fprintf(fEngineLog, "%u heartbeat %u dt=%u ms\n", t, dif, dt);
					}
					if (dt > 200) {
						fprintf(fEngineLog, "WARNING: %u heartbeat %u dt=%u ms\n", t, dif, dt);
					}
				}
				usleep(1000);
			}
			counter++;

			//if (counter % 10 == 0) fprintf(stderr, "ACI DEBUG: GPS latitude = %d\n", gpsPosition.longitude);
		}
#if 1
		else {
			if (result == 0) {
				usleep(1);
			}
		}
#endif
	}
	//if (fByteDebugLog) fclose(fByteDebugLog);
	ROS_DEBUG("ACI thread interrupted");
}

void CAsctecUSB::versionsUpdated(struct ACI_INFO aciInfo)
{
	printf("******************** Versions *******************\n");
	printf("* Type\t\t\tDevice\t\tRemote\t*\n");
	printf("* Major version\t\t%d\t=\t\%d\t*\n",aciInfo.verMajor,ACI_VER_MAJOR);
	printf("* Minor version\t\t%d\t=\t\%d\t*\n",aciInfo.verMinor,ACI_VER_MINOR);
	printf("* MAX_DESC_LENGTH\t%d\t=\t\%d\t*\n",aciInfo.maxDescLength,MAX_DESC_LENGTH);
	printf("* MAX_NAME_LENGTH\t%d\t=\t\%d\t*\n",aciInfo.maxNameLength,MAX_NAME_LENGTH);
	printf("* MAX_UNIT_LENGTH\t%d\t=\t\%d\t*\n",aciInfo.maxUnitLength,MAX_UNIT_LENGTH);
	printf("* MAX_VAR_PACKETS\t%d\t=\t\%d\t*\n",aciInfo.maxVarPackets,MAX_VAR_PACKETS);
	printf("*************************************************\n");
}

void CAsctecUSB::varListUpdateFinished()
{
	ROS_INFO("List of variables updated");

	if (instance) {
		instance->initializeVariables();
	}
}

void CAsctecUSB::cmdListUpdateFinished()
{
	ROS_INFO("Command list received");
	if (instance) {
		instance->initializeCommands();
	}
}

void CAsctecUSB::paramListUpdateFinished()
{
	ROS_INFO("List of parameters updated");
	if (instance) {
		fprintf(stderr, "init params\n");
		instance->initializeParameters();
	}
}

void CAsctecUSB::varPacketReceived(unsigned char packetNumber)
{
	if (instance) {
		instance->onVarPacketReceived(packetNumber);
	}
}

void CAsctecUSB::transmit(void* byte, unsigned short cnt)
{
	if (instance) {
#ifdef ACI_DEBUG
//#if 1
		fprintf(stderr, "ACI WRITE [%d]:", cnt);
		for (int i = 0; i < cnt; i++) {
			fprintf(stderr, " %02X", (unsigned char) ((char*)byte)[i]);
		}
		fprintf(stderr, "\n");
//#endif
		if (instance->fd < 0) fprintf(stderr, "WARNING: transmit(len=%u) fd=%d\n", cnt, instance->fd);
#endif
		unsigned char *tbyte = (unsigned char *) byte;
		for (int i = 0; i < cnt; i++) {
			write(instance->fd, &tbyte[i], 1);
		}
	}
}

void CAsctecUSB::initializeCommands()
{
	ctrlMode = 0; // 0=DIMC, 1=DMC, 2=ATT, 3=GPS
	ctrlEnabled = 0; // 0=disabled, 1=enabled
	aciResetCmdPacketContent(CMDPACKET_DEFAULT);
	aciAddContentToCmdPacket(CMDPACKET_DEFAULT, 0x0600, &ctrlMode);
	aciAddContentToCmdPacket(CMDPACKET_DEFAULT, 0x0601, &ctrlEnabled);
	aciAddContentToCmdPacket(CMDPACKET_DEFAULT, 0x50A, &ctrlPitchInput);
	aciAddContentToCmdPacket(CMDPACKET_DEFAULT, 0x50B, &ctrlRollInput);
	aciAddContentToCmdPacket(CMDPACKET_DEFAULT, 0x50C, &ctrlYawInput);
	aciAddContentToCmdPacket(CMDPACKET_DEFAULT, 0x50D, &ctrlThrustInput);
	aciAddContentToCmdPacket(CMDPACKET_DEFAULT, 0x50E, &ctrlInputCtrl);
	aciAddContentToCmdPacket(CMDPACKET_DEFAULT, 0x506, &dmcPitch);
	aciAddContentToCmdPacket(CMDPACKET_DEFAULT, 0x507, &dmcRoll);
	aciAddContentToCmdPacket(CMDPACKET_DEFAULT, 0x508, &dmcYaw);
	aciAddContentToCmdPacket(CMDPACKET_DEFAULT, 0x509, &dmcThrust);

	aciSendCommandPacketConfiguration(CMDPACKET_DEFAULT, 0);
	aciUpdateCmdPacket(CMDPACKET_DEFAULT);

	waypointUpdate = 0;
	aciResetCmdPacketContent(CMDPACKET_WAYPOINT);
	aciAddContentToCmdPacket(CMDPACKET_WAYPOINT, 0x720, &waypointUpdate);
	aciAddContentToCmdPacket(CMDPACKET_WAYPOINT, 0x721, &waypointLatitude);
	aciAddContentToCmdPacket(CMDPACKET_WAYPOINT, 0x722, &waypointLongitude);
	aciAddContentToCmdPacket(CMDPACKET_WAYPOINT, 0x723, &waypointAltitude);
	aciAddContentToCmdPacket(CMDPACKET_WAYPOINT, 0x724, &waypointHeading);
	aciAddContentToCmdPacket(CMDPACKET_WAYPOINT, 0x725, &waypointIndex);
	aciSendCommandPacketConfiguration(CMDPACKET_WAYPOINT, 0);
	aciUpdateCmdPacket(CMDPACKET_WAYPOINT);

	/*
	aciAddContentToCmdPacket(0, 0x0500, &motor1);
	aciAddContentToCmdPacket(0, 0x0501, &motor2);
	aciAddContentToCmdPacket(0, 0x0502, &motor3);
	aciAddContentToCmdPacket(0, 0x0503, &motor4);
	aciAddContentToCmdPacket(0, 0x0504, &motor5);
	aciAddContentToCmdPacket(0, 0x0505, &motor6);
	aciAddContentToCmdPacket(1, 0x0600, &ctrlMode);
	aciAddContentToCmdPacket(1, 0x0601, &ctrlEnabled);
	aciAddContentToCmdPacket(1, 0x0602, &disable_motor_onoff_by_stick);
	aciSendCommandPacketConfiguration(0, 0);
	aciSendCommandPacketConfiguration(1, 1);
	motor1 = 0;
	motor2 = 0;
	motor3 = 0;
	motor4 = 0;
	motor5 = 0;
	motor6 = 0;
	ctrlMode=0; // direct individual motor control (DIMC)
	ctrlEnabled=1;
	disable_motor_onoff_by_stick=1; // enable motors start by stick (min thrust + full yaw)
	//disable_motor_onoff_by_stick=0; // disable motors start by stick (min thrust + full yaw)
	aciUpdateCmdPacket(0);
	aciUpdateCmdPacket(1);
	*/
	simGpsData.mode = 0;
	simGpsData.update = 0;
	simGpsData.latitude = 0;
	simGpsData.longitude = 0;
	simGpsData.altitude = 0;
	simGpsData.heading = 0;
	aciResetCmdPacketContent(CMDPACKET_SIMGPS);
	aciAddContentToCmdPacket(CMDPACKET_SIMGPS, 0x760, &simGpsData.mode);
	aciAddContentToCmdPacket(CMDPACKET_SIMGPS, 0x761, &simGpsData.update);
	aciAddContentToCmdPacket(CMDPACKET_SIMGPS, 0x762, &simGpsData.latitude);
	aciAddContentToCmdPacket(CMDPACKET_SIMGPS, 0x763, &simGpsData.longitude);
	aciAddContentToCmdPacket(CMDPACKET_SIMGPS, 0x764, &simGpsData.altitude);
	aciAddContentToCmdPacket(CMDPACKET_SIMGPS, 0x765, &simGpsData.heading);
	aciSendCommandPacketConfiguration(CMDPACKET_SIMGPS, 0);
	aciUpdateCmdPacket(CMDPACKET_SIMGPS);

	commandsReady = true;
}

void CAsctecUSB::initializeVariables()
{
	aciResetVarPacketContent(0);
	aciAddContentToVarPacket(0, 0x0300, &attitude.anglePitch); // raw units [1000 * deg]
	aciAddContentToVarPacket(0, 0x0301, &attitude.angleRoll); // raw units [1000 * deg]
	aciAddContentToVarPacket(0, 0x0302, &attitude.angleYaw); // raw units [1000 * deg]

	if (readMotorRpm) {
		fprintf(stderr, "enabling reading of motor RPM\n");
		aciAddContentToVarPacket(0, 0x0100, &(motorRPM[0])); // raw units [0..200]
		aciAddContentToVarPacket(0, 0x0101, &(motorRPM[1]));
		aciAddContentToVarPacket(0, 0x0102, &(motorRPM[2]));
		aciAddContentToVarPacket(0, 0x0103, &(motorRPM[3]));
		aciAddContentToVarPacket(0, 0x0104, &(motorRPM[4]));
		aciAddContentToVarPacket(0, 0x0105, &(motorRPM[5]));
	}

	aciResetVarPacketContent(1);
	aciAddContentToVarPacket(1, 0x0110, &gpsPosition.status);
	if (!noGpsMode) {
		aciAddContentToVarPacket(1, 0x0106, &gpsPosition.latitude);
		aciAddContentToVarPacket(1, 0x0107, &gpsPosition.longitude);
		aciAddContentToVarPacket(1, 0x0108, &gpsPosition.altitude);
		aciAddContentToVarPacket(1, 0x010B, &gpsPosition.heading);
		aciAddContentToVarPacket(1, 0x010F, &gpsPosition.satNum);

		if (!readMotorRpm) { // save some data rate when reading motor RPM
			aciAddContentToVarPacket(1, 0x0303, &fusedPosition.latitude);
			aciAddContentToVarPacket(1, 0x0304, &fusedPosition.longitude);
			aciAddContentToVarPacket(1, 0x0306, &fusedPosition.altitude);
		}
	}

	aciResetVarPacketContent(2);
	aciAddContentToVarPacket(2, 0x0001, &uavStatus);
	aciAddContentToVarPacket(2, 0x0002, &uavFlightTime);
	aciAddContentToVarPacket(2, 0x0003, &uavBatteryVoltage);

	aciAddContentToVarPacket(2, 0x0600, &rcChannel[0]);
	aciAddContentToVarPacket(2, 0x0601, &rcChannel[1]);
	aciAddContentToVarPacket(2, 0x0602, &rcChannel[2]);
	aciAddContentToVarPacket(2, 0x0603, &rcChannel[3]);
	aciAddContentToVarPacket(2, 0x0604, &rcChannel[4]);
	aciAddContentToVarPacket(2, 0x0605, &rcChannel[5]);
	aciAddContentToVarPacket(2, 0x0606, &rcChannel[6]);
	aciAddContentToVarPacket(2, 0x0607, &rcChannel[7]);

	//aciAddContentToVarPacket(2, 0x0111, &gpsTimeOfWeek);

	fprintf(stderr, "variable period: %d\n", variableRequestPeriodMs);
	aciSetVarPacketTransmissionRate(0, variableRequestPeriodMs);
	//aciVarPacketUpdateTransmissionRates();
	aciSetVarPacketTransmissionRate(1, variableRequestPeriodMs);
	if (noGpsMode) {
		fprintf(stderr, "WARNING: GPS reading was disabled !!!\n");
	}
	aciSetVarPacketTransmissionRate(2, variableRequestPeriodMs);
	if (fEngineLog) {
		fprintf(fEngineLog, "%u aciSetVarPacketTransmissionRate(0..2, %u) rate=%u Hz period=%u ms\n", getTimeMs(), variableRequestPeriodMs, INTERNAL_ENGINE_RATE_HZ / variableRequestPeriodMs, 1000 * variableRequestPeriodMs / INTERNAL_ENGINE_RATE_HZ);
	}
	aciVarPacketUpdateTransmissionRates();
	usleep(10000);
	aciSendVariablePacketConfiguration(0);
	usleep(10000);
	aciSendVariablePacketConfiguration(1);
	usleep(10000);
	aciSendVariablePacketConfiguration(2);
	usleep(10000);

	if (fEngineLog) {
		fprintf(fEngineLog, "%u aciGetVarPacketRate(0) = %u\n", getTimeMs(), aciGetVarPacketRate(0));
		fprintf(fEngineLog, "%u aciGetVarPacketRate(1) = %u\n", getTimeMs(), aciGetVarPacketRate(1));
		fprintf(fEngineLog, "%u aciGetVarPacketRate(2) = %u\n", getTimeMs(), aciGetVarPacketRate(2));
	}

	variablesReady = true;
}

void CAsctecUSB::initializeParameters()
{
	aciResetParPacketContent(0);
	//aciAddContentToParamPacket(0,0x0001,&bat);
	aciAddContentToParamPacket(0, 0x0720, &paramMaxCruiseSpeed);
	aciAddContentToParamPacket(0, 0x0721, &paramPositionAccuracy);
	aciSendParameterPacketConfiguration(0);
	parametersReady = true;
}

void CAsctecUSB::onVarPacketReceived(unsigned char packetNumber)
{
	// if (debugLevel > 2) {
	// 	fprintf(stderr, "%u variable packet (%u) received\n", getTimeMs(), packetNumber);
	// }

	ROS_DEBUG("%u variable packet (%u) received", getTimeMs(), packetNumber);

	if (noGpsMode) {
		if (packetNumber == 0) {
			internalDataUpdated = true;
		}
	} else {
		if (packetNumber == 1) {
			internalDataUpdated = true;
		}
	}
	if (fEngineLog) fprintf(fEngineLog, "%u var packet %d received\n", getTimeMs(), packetNumber);
}

void CAsctecUSB::sendSimGps(double latitude, double longitude, double altitude, double headingDeg)
{
	if (debugLevel > 0) fprintf(stderr, "send SIM GPS ...\n");
	simGpsData.latitude = (int32_t) (1e7 * latitude);
	simGpsData.longitude = (int32_t) (1e7 * longitude);
	simGpsData.altitude = (int32_t) (1e3 * altitude);
	simGpsData.heading = (int32_t) (1e3 * headingDeg);
	simGpsData.mode = 1;
	simGpsData.update = 1;
	aciUpdateCmdPacket(CMDPACKET_SIMGPS);
}

void CAsctecUSB::stopSimGps()
{
	simGpsData.mode = 0;
	simGpsData.update = 1;
	aciUpdateCmdPacket(CMDPACKET_SIMGPS);
}

bool CAsctecUSB::arm()
{
	// TODO: how to arm? send setControlAttitude(0, 0, -1, 0) and hold it for 1 second; need to check the actual armed/disarmed state!
	// arm = true;
	
	return false;
}

bool CAsctecUSB::disarm()
{
	// TODO: how to disarm?
	// arm = false;

	return false;
}

void CAsctecUSB::enableCtrl()
{
	ctrlEnabled = 1;

	aciUpdateCmdPacket(CMDPACKET_DEFAULT);
	if (fEngineLog) {
		fprintf(fEngineLog, "%u control enabled: ctrlEnabled=%u ctrlMode=%u\n", getTimeMs(), ctrlEnabled, ctrlMode);
	}
}

void CAsctecUSB::disableCtrl()
{
	ctrlEnabled = 0;

	aciUpdateCmdPacket(CMDPACKET_DEFAULT);
	if (fEngineLog) {
		fprintf(fEngineLog, "%u control disabled: ctrlEnabled=%u ctrlMode=%u\n", getTimeMs(), ctrlEnabled, ctrlMode);
	}
}

bool CAsctecUSB::setControlMode(TAsctecControlMode mode)
{
	switch(mode) {
	case ASCTEC_CONTROL_NONE:
		ctrlEnabled = 0;
		ctrlMode = 0;
		break;
	case ASCTEC_CONTROL_MOTORS:
		//ctrlEnabled = 1;
		ctrlMode = 0;
		break;
	case ASCTEC_CONTROL_DIRECT:
		//ctrlEnabled = 1;
		ctrlMode = 1;
		break;
	case ASCTEC_CONTROL_HEIGHT:
		//ctrlEnabled = 1;
		ctrlMode = 2;
		break;
	case ASCTEC_CONTROL_POSITION:
		//ctrlEnabled = 1;
		ctrlMode = 3;
		break;
	default:
		return false;
	}
	aciUpdateCmdPacket(CMDPACKET_DEFAULT);
	if (fEngineLog) {
		fprintf(fEngineLog, "%u set control mode (%d) ctrlEnabled=%u ctrlMode=%u\n", getTimeMs(), mode, ctrlEnabled, ctrlMode);
	}
	return true;
}

bool CAsctecUSB::setControlDirect(SControlValues control)
{
	// input values are <-1;1>, thrust <0;1>
	int iPitch = 100 + (int) (100 * control.elevator);
	if (iPitch < 0) iPitch = 0; else if (iPitch > 200) iPitch = 200;
	int iRoll = 100 + (int) (100 * control.aileron);
	if (iRoll < 0) iRoll = 0; else if (iRoll > 200) iRoll = 200;
	int iYaw = 100 + (int) (100 * control.rudder);
	if (iYaw < 0) iYaw = 0; else if (iYaw > 200) iYaw = 200;
	int iThrust = (int) (200 * control.thrust);
	if (iThrust < 0) iThrust = 0; else if (iThrust > 200) iThrust = 200;
	dmcPitch = (uint8_t) iPitch;
	dmcRoll = (uint8_t) iRoll;
	dmcYaw = (uint8_t) iYaw;
	dmcThrust = (uint8_t) iThrust; aciUpdateCmdPacket(CMDPACKET_DEFAULT);
	dmcThrust = 200;
	return false;
}

static int16_t checkLimits(int value, int minLimit, int maxLimit)
{
	if (value < minLimit) value = minLimit;
	else
	if (value > maxLimit) value = maxLimit;
	return (int16_t) value;
}

#define MAX_ATTITUDE_ANGLE_RAD (51.2 * M_PI / 180.0)
#define MAX_ATTITUDE_YAW_VEL_RAD (51.2 * M_PI / 180.0) // TODO: unknown yaw-velocity factor

bool CAsctecUSB::setControlAttitude(SControlValues control)
{
	// angle unit: radians (pitch / roll), rad/s (yaw) and 0-1 thrust
	ctrlPitchInput = checkLimits(2047.0 * control.elevator / MAX_ATTITUDE_ANGLE_RAD, -2047, 2047);
	ctrlRollInput = checkLimits(2047.0 * control.aileron / MAX_ATTITUDE_ANGLE_RAD, -2047, 2047);
	ctrlYawInput = checkLimits(2047.0 * control.rudder / MAX_ATTITUDE_YAW_VEL_RAD, -2047, 2047);
	ctrlThrustInput = checkLimits(4090.0 * control.thrust, 0, 4090);
	// ctrlInputCtrl = 0x1F; // enable vertical velocity control
	ctrlInputCtrl = CTRL_THRUST; // enable thrust control

	// printf("%u set control mode (%d) ctrlEnabled=%u ctrlMode=%u\n", getTimeMs(), ctrlMode, ctrlEnabled, ctrlMode);
	// printf("%u setControlAttitude(%d, %d, %d, %d)\n", getTimeMs(), ctrlPitchInput, ctrlRollInput, ctrlYawInput, ctrlThrustInput);

	aciUpdateCmdPacket(CMDPACKET_DEFAULT);
	if (fEngineLog) {
		fprintf(fEngineLog, "%u setControlAttitude(%d, %d, %d, %d)\n", getTimeMs(), ctrlPitchInput, ctrlRollInput, ctrlYawInput, ctrlThrustInput);
	}
	return true;
}

bool CAsctecUSB::setControlPosition(SGpsSetPoint setPoint, uint32_t * pIndex)
{
	// lat/lon [deg], alt[m], yaw[rad]
	uint32_t index = ++waypointIndexGenerator;
	if (pIndex) *pIndex = index;
	waypointLatitude = (int32_t) (setPoint.latitude * 1e7);
	waypointLongitude = (int32_t) (setPoint.longitude * 1e7);
	int32_t altitudeToHeightCorrection = fusedPosition.altitude - gpsPosition.altitude;
	waypointAltitude = (int32_t) (setPoint.altitude * 1e3) + altitudeToHeightCorrection;
	waypointHeading = !::isnan(setPoint.azimuth) ? (int32_t) (setPoint.azimuth * 1e3) : 0; // TODO: change NAN to 0x7FFFFFFF after firmware update in asctec
	//waypointHeading = !::isnan(yaw) ? (int32_t) (yaw * 1e3) : 0x7FFFFFFF;
	waypointIndex = index;
	waypointUpdate = 1;
	aciUpdateCmdPacket(CMDPACKET_WAYPOINT);
	return true;
}

bool CAsctecUSB::getAttitude(CAttitudeRaw & resultAttitude)
{
	resultAttitude = attitude;
	return true;
}

bool CAsctecUSB::getGpsPosition(CGpsDataRaw & resultPosition)
{
	resultPosition = gpsPosition;
	return true;
}

bool CAsctecUSB::getFusedPosition(CGpsDataRaw & resultPosition)
{
	resultPosition = fusedPosition;
	return true;
}

void CAsctecUSB::setIMUCallback(void (*callback)(CAttitude&, void*), void* uInput){
	IMUCallback = callback;
	IMUCallInput = uInput;
}

void CAsctecUSB::setRCCallback(void (*callback)(SControlValues&, void*), void* uInput){
	RCCallback = callback;
	RCCallInput = uInput;
}

void CAsctecUSB::setStateCallback(void (*callback)(SState&, void*), void* uInput){
	StateCallback = callback;
	StateCallInput = uInput;
}

/* end of asctec_usb.cc */
